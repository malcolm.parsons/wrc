use crate::inner::Inner;
use crate::weight::Weight;
use std::usize::MAX;

#[test]
fn new_assigns_starting_weight() {
    let inner = Inner::new(13, 5);
    assert_eq!(inner.get_weight(), 5);
}

#[test]
fn weight_can_be_added() {
    let inner = Inner::new(13, 5);
    inner.add_weight(17).unwrap();
    assert_eq!(inner.get_weight(), 22);
}

#[test]
fn weight_add_doesnt_overflow() {
    let inner = Inner::new(13, MAX - 10);
    assert!(inner.add_weight(20).is_none())
}

#[test]
fn weight_can_be_dropped() {
    let inner = Inner::new(13, 5);
    inner.drop_weight(2).unwrap();
    assert_eq!(inner.get_weight(), 3);
}

#[test]
fn weight_drop_doesnt_overflow() {
    let inner = Inner::new(13, 5);
    assert!(inner.drop_weight(10).is_none());
}
