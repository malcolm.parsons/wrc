use crate::WRC;
use std::sync::Mutex;
use std::thread;

#[test]
fn wrcs_can_be_sent_between_threads() {
    let wrc0 = WRC::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let wrc = wrc0.clone();
        let handle = thread::spawn(move || {
            let mut num = wrc.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    assert_eq!(*wrc0.lock().unwrap(), 10);
}
