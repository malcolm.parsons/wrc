use crate::WRC;
use std::cmp::Ordering;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[test]
fn implements_fmt_debug() {
    let example = WRC::new((1, 2, 3));
    assert_eq!(format!("{:?}", example), "(1, 2, 3)");
}

#[test]
fn implemnts_fmt_display() {
    let example = WRC::new(13);
    assert_eq!(format!("{}", example), "13");
}

#[test]
fn implements_partial_eq() {
    let wrc0 = WRC::new((1, 2, 3));
    let wrc1 = wrc0.clone();
    assert_eq!(wrc0, wrc1);
}

#[test]
fn implements_partial_ord() {
    let wrc0 = WRC::new(13);
    let wrc1 = WRC::new(27);

    assert!(wrc0 < wrc1);
    assert!(wrc0 <= wrc1);
    assert!(wrc1 > wrc0);
    assert!(wrc1 >= wrc0);
}

#[test]
fn implements_ord() {
    let wrc0 = WRC::new(13);
    let wrc1 = WRC::new(27);
    assert_eq!(wrc0.cmp(&wrc1), Ordering::Less);
    assert_eq!(wrc1.cmp(&wrc0), Ordering::Greater);
    assert_eq!(wrc0.cmp(&wrc0), Ordering::Equal);
}

#[test]
fn implements_default() {
    let wrc0: WRC<usize> = Default::default();
    let wrc1 = WRC::new(0);
    assert_eq!(wrc0, wrc1);
}

#[test]
fn implements_hash() {
    let wrc = WRC::new(13);
    let mut hash0 = DefaultHasher::new();
    wrc.hash(&mut hash0);
    let mut hash1 = DefaultHasher::new();
    13.hash(&mut hash1);
    assert_eq!(hash0.finish(), hash1.finish());
}
