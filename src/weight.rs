pub trait Weight {
    /// Add weight to the existing weight value, with overflow checking.
    fn add_weight(&self, _: usize) -> Option<usize>;

    /// Subtract weight to the existing weight value, with overflow checking.
    fn drop_weight(&self, _: usize) -> Option<usize>;

    /// Get the weight of the value.
    fn get_weight(&self) -> usize;
}
