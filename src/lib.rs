//! # WRC
//!
//! A thread-safe weight reference counting smart-pointer for Rust.

mod inner;
mod weight;
mod wrc;

pub use crate::wrc::WRC;

#[cfg(test)]
mod test;
