#!/bin/sh

docker run --rm -v "$(pwd)":/app -w /app rustlang/rust:nightly sh -c "cargo $@"
