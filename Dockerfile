FROM rustlang/rust:nightly
ADD . /app
WORKDIR /app
CMD ["cargo", "test"]
