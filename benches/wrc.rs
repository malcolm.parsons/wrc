#[macro_use]
extern crate criterion;
extern crate wrc;

use criterion::Criterion;
use wrc::WRC;

#[allow(unused_must_use)]
fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("wrc 32", |b| {
        b.iter(|| {
            let smart_pointer = WRC::new("Marty McFly".to_string());
            for _ in 0..32 {
                let clone = smart_pointer.clone();
                drop(clone);
            }
        });
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
