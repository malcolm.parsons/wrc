#[macro_use]
extern crate criterion;

use criterion::Criterion;
use std::sync::Arc;

#[allow(unused_must_use)]
fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("arc 32", |b| {
        b.iter(|| {
            let smart_pointer = Arc::new("Marty McFly".to_string());
            for _ in 0..32 {
                let clone = smart_pointer.clone();
                drop(clone);
            }
        });
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
